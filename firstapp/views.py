from .models import Post, Category
from .serializers import PostSerializer, CategorySerializer, UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from firstapp.permissions import IsOwnerOrReadOnly

# Create your views here.


class PostList(generics.ListCreateAPIView):
    # NOTE : serializer_class  &  queryset  VARIABLE NAMES PREdefined h in listcreateAPIview de vich ,ena de jga hor name ni le sakde apa
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserList(generics.ListAPIView):
    # NOTE 1: ena te mae koi permision ni layi but je mae chundi k bydefault lag jawe , ode lyi mnu settings.py  file ch
    # file ch tu DEFAULT_PERMISSION_CLASSES  toh comment hta k dekh te fr users(http://127.0.0.1:8000/users/) vale webpage te check kr

    # NOTE 2 : WEBAPGE TE SIRF DATA sHOW ho rha but get ,options,post etc ni show rha REASON IS :generics.ListAPIView use kita ede ch bes mae
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):

    # NOTE 2 : webpage te sirf data show ho rha but delete ,put vgera de option show ni ho rhi REASON IS : mae ede ch
    # generics.RetrieveAPIView eh use kita bes
    queryset = User.objects.all()
    serializer_class = UserSerializer
