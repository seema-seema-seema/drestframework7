from django.urls import path
from .views import PostList, PostDetail, UserDetail, UserList
from rest_framework.urlpatterns import format_suffix_patterns
# from firstapp import views ,UPER VALI LINE NU EDA V LIKH SAKDE A ASI ede lyi fr niche eda likhna pena path('', views.post_list, name='list'),


urlpatterns = [
    path('users/', UserList.as_view()),
    path('users/<int:pk>/', UserDetail.as_view()),
    path('posts/', PostList.as_view()),
    path('posts/<int:pk>/', PostDetail.as_view()),  # pk de jga id v likh SAKDE
]
urlpatterns = format_suffix_patterns(urlpatterns)
