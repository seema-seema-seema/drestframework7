from rest_framework import serializers
from .models import Category, Post
from django.contrib.auth.models import User


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        # likhna jrori becuz error a rhi k eh version deprecateho gya eh line likho ta likya
        fields = '__all__'

        # eda v kr sakde jive modl form ch krde hi apa
        #fields = ['id', 'title','created_at']


class PostSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Post
        fields = '__all__'

        #fields = ['id', 'title']


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Post.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'posts']
